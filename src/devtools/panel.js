(function() {
"use strict";

    var Panel = function() {
        this.eventsList = [];
        this.tabId = chrome.devtools.inspectedWindow.tabId;
    };

    Panel.prototype.init = function() {
        chrome.extension.sendRequest(
            {
                'action': 'GetAll',
                'data': {'tabId': this.tabId }
            },
            function getAll(result) {
                this.eventsList = result.data;
                this.redrawTable();
            }.bind(this)
        );
    };

    Panel.prototype.redrawTable = function() {
        var event, row;
        var tableBody = $("#eventTable > tbody");
        tableBody.empty();

        for (var i = 0, count = this.eventsList.length; i < count; i++) {
            event = this.eventsList[i];

            row = $("<tr/>");
            row.append($("<td/>").addClass('hiddenColumn').text(i));
            row.append($("<td/>").text(event.elementInfo));
            row.append($("<td/>").text(event.eventName));
            row.append($("<td/>").text(event.eventLength));
            row.append($("<td/>").addClass('hiddenColumn').text(event.elementId));

            row.addClass('event-row');
            row.on('click', this.inspect.bind(this, event.elementId));
            row.on('mouseover', this.highlight.bind(this, event.elementId));
            row.on('mouseout', this.unHighlight.bind(this, event.elementId));

            tableBody.append(row);
        }
    };

    Panel.prototype.inspect = function(smellId) {
          chrome.devtools.inspectedWindow.eval(
            'inspect(document.querySelector(\'[data-smells-id="' + smellId + '"]\'));'
          );
    };

    Panel.prototype.highlight = function(smellId) {
        chrome.devtools.inspectedWindow.eval(
            'var element = document.querySelector(\'[data-smells-id="' + smellId + '"]\');' +
            'if (element.tagName != "body") { /*element.scrollIntoView();*/ element.style.outline = "2px dashed #f00";}'
          );
    };

    Panel.prototype.unHighlight = function(smellId) {
        chrome.devtools.inspectedWindow.eval(
            'var element = document.querySelector(\'[data-smells-id="' + smellId + '"]\');' +
            'if (element.tagName != "body") { element.style.outline = "";}'
          );
    };

    var panel = new Panel();
    panel.init();
})();