(function() {
"use strict";

    var Popup = function() {
      this.tabId = null;
    };

    Popup.prototype.init = function(smellsHtml, tabId) {
        document.getElementById('smells').innerHTML = smellsHtml;
        this.tabId = tabId;

        [].forEach.call(document.querySelectorAll('#smells > [role="element\\-shower"]'), function attachEvents(element) {
            element.addEventListener(
                'click',
                this.showerHandler.bind(this)
            );
        }, this);

        document.getElementById('show-in-console').addEventListener(
            'click',
            this.sendMessage.bind(this, 'showAll', {})
        );

        document.getElementById('clear').addEventListener(
            'click',
            this.clearHandler.bind(this)
        );
    };

    Popup.prototype.showerHandler = function(event) {
        this.sendMessage('show', {
            'id': event.target.getAttribute('data-element-id'),
            'eventName': event.target.getAttribute('data-event-name')
        });
    };

    Popup.prototype.clearHandler = function() {
        document.getElementById('smells').innerHTML = '';
        this.sendMessage('clear');
        chrome.pageAction.hide(this.tabId);
        window.close();
    };

    Popup.prototype.sendMessage = function(action, data) {
        chrome.tabs.sendMessage(this.tabId, {
            'action': action,
            'data': data
        });
    };

    var popup = new Popup();

    document.addEventListener('DOMContentLoaded', popup.init.bind(
        popup,
        decodeURIComponent(new RegExp('.*?\\?smells=(.+)&', 'g').exec(window.location.href)[1]),
        +decodeURIComponent(new RegExp('.*?\\&tabId=(.+)', 'g').exec(window.location.href)[1])
    ), false);
})();